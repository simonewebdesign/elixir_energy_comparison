# Elixir Energy Comparison CLI tool

## Try it

The only requirement is Erlang 19 installed on your machine.

Open a terminal and run:

    $ bin/comparison test/fixtures/plans.json < test/fixtures/inputs


## Compiling from source

You need Elixir 1.3 or newer.

    $ mix deps.get
    $ env MIX_ENV=prod mix escript.build

This should produce the `bin/comparison` executable.


## Development

You can watch files for changes, recompile and run the tests automatically with:

    $ mix test.watch

To run the tests only:

    $ mix test

Static analysis tools you can use:

    $ mix dialyzer
    $ mix dogma
    $ mix credo --strict
