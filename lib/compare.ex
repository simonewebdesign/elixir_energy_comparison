defmodule Compare do
  @moduledoc """
  This module contains the core business logic for the possible commands
  """
  @vat 0.05

  @doc """
  Produces an annual price in pence for the provided plan, inclusive of VAT.
  """
  @spec price(%Plan{}, number) :: integer
  def price(plan, annual_usage) do
    Enum.reduce(plan.rates, {0, annual_usage}, fn(rate, {cost, consumption}) ->
      {threshold, price} = {rate.threshold, rate.price}
      if threshold do
        new_cost = cost + price * threshold
        new_consumption = consumption - threshold
        {new_cost, new_consumption}
      else
        final_cost = cost + price * consumption + plan.standing_charge * 365
        final_cost + final_cost * @vat |> round
      end
    end)
  end

  @doc """
  For the specified plan it calculates how much energy (in kWh)
  would be used annually from a monthly spend in pounds, inclusive of VAT.
  """
  @spec usage(%Plan{}, number) :: integer
  def usage(plan, monthly_spend) do
    annual_spend = monthly_spend / (1 + @vat) * 12

    Enum.reduce(plan.rates, {0, annual_spend}, fn(rate, {usage, spend}) ->
      {threshold, price} = {rate.threshold, rate.price}
      if threshold do
        new_usage = usage + threshold
        new_spend = spend - threshold * price / 100
        {new_usage, new_spend}
      else
        final_spend = spend - plan.standing_charge * 365 / 100
        usage + final_spend / (price / 100) |> round
      end
    end)
  end
end
