defmodule Comparison do
  @moduledoc """
  This executable will receive the input commands on stdin and will be passed a
  single argument that is the full path to plans.json.
  For example:

      $ ./bin/comparison /path/to/plans.json < inputs

  """
  @type command :: list

  @spec main(list(binary)) :: :ok
  def main([]), do: IO.puts "usage: comparison /path/to/plans.json"
  def main([path]) do
    File.read!(path)
    |> Poison.decode!(as: [%Plan{}])
    |> read_stdio
  end

  @spec read_stdio([%Plan{}]) :: :ok
  defp read_stdio(plans) do
    Enum.each IO.stream(:stdio, :line), &parse_line(&1, plans)
  end

  @spec parse_line(binary, [%Plan{}]) :: :ok
  defp parse_line("exit\n", _), do: exit(:shutdown)
  defp parse_line(line, plans) do
    Parser.parse(line)
    |> evaluate(plans)
  end

  @spec evaluate(command, [%Plan{}]) :: :ok
  defp evaluate(["price", annual_usage], plans) do
    plans
    |> Enum.map(fn p -> [p.supplier, p.plan, Currency.format(Compare.price p, annual_usage)] end)
    |> Enum.sort_by(&List.last/1)    # sort by price
    |> Enum.map(&Enum.join &1, ",")  # add commas
    |> Enum.join("\n")               # add newlines
    |> IO.puts                       # print it
  end

  defp evaluate(["usage", supplier_name, plan_name, monthly_spend], plans) do
    for plan = %Plan{supplier: ^supplier_name, plan: ^plan_name} <- plans, do:
      IO.puts Compare.usage(plan, monthly_spend)
  end
end
