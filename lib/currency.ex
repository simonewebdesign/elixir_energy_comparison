defmodule Currency do
  @moduledoc """
  Utility functions for working with currencies
  """

  @doc """
  Formats pence as pound, cents as dollars etc.

  ## Examples

      iex> Currency.format 1
      "0.01"

      iex> Currency.format 12
      "0.12"

      iex> Currency.format 123
      "1.23"

      iex> Currency.format 10000
      "100.00"
  """
  @spec format(integer, String.t) :: String.t
  def format(amount, delimiter \\ ".") do
    super_unit = div(abs(amount), 100)
    sub_unit = rem(abs(amount), 100) |> Integer.to_string |> String.rjust(2, ?0)
    [super_unit, sub_unit] |> Enum.join(delimiter)
  end
end
