defmodule Parser do
  @moduledoc "Module for parsing command lines"
  use Combine

  @doc ~S"""
  Parses the given `line` into a command.

  ## Examples

      iex> Parser.parse "price 1000\n"
      ["price", 1000]

      iex> Parser.parse "usage edf fixed 350\n"
      ["usage", "edf", "fixed", 350]

      iex> Parser.parse "usage bg standing-charge 120\n"
      ["usage", "bg", "standing-charge", 120]
  """
  @spec parse(binary) :: list
  def parse(line), do: Combine.parse(line, parser)

  defp parser, do: either price, usage

  defp price do
    string("price")
    |> ignore(space)
    |> integer
    |> ignore(newline)
  end

  defp usage do
    string("usage")
    |> ignore(space)
    |> word
    |> ignore(space)
    |> word_of(~r/[\w-]+/)
    |> ignore(space)
    |> integer
    |> ignore(newline)
  end
end
