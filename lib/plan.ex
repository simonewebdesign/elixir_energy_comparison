defmodule Plan do
  defstruct [:plan, :supplier, rates: [%Rate{}], standing_charge: 0]
end
