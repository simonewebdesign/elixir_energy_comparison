defmodule Comparison.Mixfile do
  use Mix.Project

  def project do
    [app: :comparison,
     version: "1.0.0",
     elixir: "~> 1.3",
     escript: escript,
     deps: deps]
  end

  defp escript do
    [main_module: Comparison, path: "bin/comparison"]
  end

  defp deps do
    [{:dogma, "~> 0.1", only: :dev},
     {:credo, "~> 0.4", only: [:dev, :test]},
     {:combine, "~> 0.9"},
     {:poison, "~> 3.0"},
     {:mix_test_watch, "~> 0.2", only: :dev},
     {:dialyxir, "~> 0.4", only: :dev},
    ]
  end
end
