defmodule CompareTest do
  use ExUnit.Case

  setup_all do
    {:ok, %{
      plan: "variable",
      rates: [%{price: 13.5, threshold: 100},
              %{price: 10, threshold: nil}],
      standing_charge: 0,
      supplier: "eon"}}
  end

  test "price", plan do
    assert 16_70 == Compare.price(plan, 124)
    assert 108_68 == Compare.price(plan, 1000)
    assert 213_68 == Compare.price(plan, 2000)
  end

  test "usage", plan do
    assert 13679 == Compare.usage(plan, 120)
  end
end
