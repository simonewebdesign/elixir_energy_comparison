defmodule ComparisonTest do
  use ExUnit.Case
  import ExUnit.CaptureIO

  test "generates the right output" do
    inputs = File.read!("test/fixtures/inputs")
    output = File.read!("test/fixtures/expected_output")

    stdout = capture_io(inputs, fn ->
      assert :shutdown == catch_exit(Comparison.main(["test/fixtures/plans.json"]))
    end)

    assert stdout == output
  end
end
